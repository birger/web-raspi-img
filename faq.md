---
layout: page
title: Frequently Asked Questions (FAQs) and errata
date: 2021-05-28 13:43:08 -0500
permalink: /faq/
---

In this page you can find the [Frequently Asked Questions](#faqstart)
and the pending [errata on the image creation scripts](#errata).

## Frequently Asked Questions
<a name="faqstart" />

1. **Can you add package `foo` to the image? I think it's quite
   important!**
   
   Most likely, my answer will be _no_. I am trying for the images to be
   as minimal and close to a base Debian install as possible; while
   most of us do use `console-tools` and `bash-completion` (two of the
   most common requests), it is not always required. Keep in mind the
   generated images should work for the most minimal Raspberry Pi
   model 1A (128MB RAM). And, all in all, it's just an `apt install`
   away for you anyway!
   
   If there is anything you _really_ need and cannot easily get
   done from the provided images, do contact me. But I don't want to
   change the images offered to everybody just because a feature is missing!

2. **How do I get wireless working?**

   Given we are shipping a minimal installation, I don't want to carry
   all the burden of _network-manager_, _wicd_ or the like. The
   easiest way is to create a `/etc/network/interfaces.d/wlan0` file
   with the settings for your network. You will find an example one in
   place -- All lines are commented out, as naturally, I don't have
   the settings for your wireless network.

   Of course, there might be other options you need — If that's the
   case, I suggest you look at the different options mentioned in the
   [WiFi/HowToUse page of the Debian
   Wiki](https://wiki.debian.org/WiFi/HowToUse).

3. **When I try to run `apt update`, it fails complaining that `E:
   Release file for
   http://deb.debian.org/debian/dists/buster/InRelease is not valid
   yet (invalid for another <many many days>). Updates for this
   repository will not be applied.  ` (or something similar)**

   Contrary to what's usual in the PCs, the Raspberry Pi computers do
   not ship with a battery-backed clock. This means that, when you
   first connect to the network, it will take some minutes for `ntp`
   to get the current time. I don't think there is much that can be
   done about it.

   Our images ship with `systemd-timesyncd`, which will automatically
   synchronize the clock soon after getting a network
   connection. `systemd-timesyncd` also remembers the time of its last
   synchronization, and will set the clock to just after that
   timestamp at boot time. This means, you will only get this odd
   message if you are *very* quick issuing `apt update` after setting
   up the network connection for the first time! (thanks to Paul
   Sokolovsky for this Q&A)

4. **How can I log in to my Raspberry remotely? Not every user
   installs HDMI and a keyboard...**

   Use [SSH key-based
   authentication](https://wiki.debian.org/SSH#Using_shared_keys). Once
   you heve created your key pair in your desktop system, edit your
   Raspberry's `/boot/firmware/sysconf.txt` (or just `/sysconf.txt` on
   the created image's first partition, that is, the one formatted
   with vfat), copy the contents of your user's `.ssh/id_rsa.pub` (or
   whatever name you specified for it) as the value for the
   `root_authorized_key`, like this (of course, this specific line
   would allow *me* to log in to your system)

        root_authorized_key=ssh-rsa	AAAA3NbzayC1Ec2AAAAdBAQAAAaBCQAnNYJkyqbir+KvP8GFWKoH8+aabb6ZxugDqarVhkU9vXga2z57zLSHHR7NK+a+ZuBc3ZzaD9oOwnA9FVTas3LybEM0r+BL0z / aVbgm0zhcoy4c6fq5ymkuecpyLf5jhjohft7sNh5pldaplt9lWdm0taowFzjRBin8rRrrz+uxO1n5W/vc90vsKA2/alecXS03YSk1pKDaXIEMIHL8ehQElepnPJ1J7u1071j2MgwlzSu0kM0OM5RJav5QnrFoMyr0AoJzeqFbJ4MBuCc2YXkANXXjr8NneGJiFQ4AdzgrwvRUVJ3diTNgVWEfBbxq+l93hWIoA45S0yvn5Ed7Hx gwolf@raspi.debian.org

5. **My Raspberry's LEDs are off!**

    Yes, they are off by default, but are very easy to turn on via the
    `/sys` pseudo-filesystem.  So, if you want to turn on the red
    (`power`) LED, do as root `echo 1 > /sys/class/leds/PWR/brightness`
    (and `echo 0 > /sys/class/leds/PWR/brightness` to turn it off).
    Likewise, you can control the green (`activity`) LED using
    `/sys/class/leds/ACT/brightness` in the same way.

	Besides simply turn the LEDs on or of, you can also let the LEDs
    correspond to events. What those events are may differ from model to
    model and possibly also differ per kernel version.
    On my RPi 3B+ with `arm64` kernel version `5.10.9-1` I have these:
    ```
    # cat /sys/class/leds/PWR/trigger 
[none] kbd-scrolllock kbd-numlock kbd-capslock kbd-kanalock kbd-shiftlock kbd-altgrlock kbd-ctrllock
kbd-altlock kbd-shiftllock kbd-shiftrlock kbd-ctrlllock kbd-ctrlrlock disk-activity disk-read
disk-write ide-disk mtd nand-disk cpu cpu0 cpu1 cpu2 cpu3 panic usb-gadget usb-host mmc0 mmc1
rfkill-any rfkill-none usb-001:005:01:link usb-001:005:01:1Gbps usb-001:005:01:100Mbps
usb-001:005:01:10Mbps rfkill0 bluetooth-power hci0-power rfkill1
    ```
    The ones for the `ACT` LED are the same (for me at least).  
    If you want the `PWR` LED off, but on when a kernel panic happens,
    do as root: `echo "panic" > /sys/class/leds/PWR/trigger`.  
    The active trigger is shown with `[]` around it.

6. **Are the images 32 or 64 bits?**

	The architecture chosen for each of the builds is the best suited
    for its hardware. This is one of the main differences with
    *Raspberry Pi OS*: They redefined some technical bits for the
    32-bit `armhf` architecture, allowing it to run on their least
    powerful systems (families 0 and 1), where we must run with the
    much slower (and also, 32-bit) `armel` architecture (with emulated
    floating point). And they ship 32 bit for the higher-end machines
    (3, 4) for greater consistency. Thus,

	| Family | Debian          | Raspberry Pi OS |
	|--------|-----------------|-----------------|
	| 0 / 1  | `armel`, 32 bit | `armhf`, 32 bit |
	| 2      | `armhf`, 32 bit | `armhf`, 32 bit |
	| 3      | `arm64`, 64 bit | `armhf`, 32 bit |
	| 4      | `arm64`, 64 bit | `armhf`, 32 bit |

## Errata
<a name="errata" />

1. As of late February, 2021, I fixed (mostly!) the serial console
   names specified in `/boot/firmware/cmdline.txt`. Thing is, the
   device naming for the console is not consistent across the
   Raspberry family range.

   We are now building our images using `ttyAMA0` for the images
   targetting families 1 and 2, and `ttyS1` for families 3
   and 4. However, for a reason I cannot understand, my Raspberry Pi
   0W (that uses the same images as the 1 family) has its console at
   `ttyS1`, so it won't have a usable serial console when it's first
   booted.

   Not to worry, though! At first boot time, we reconfigure the
   `raspi-firmware` package, triggering a rewrite of `cmdline.txt`
   with actual observed values. So, your RPi 0W will enable the serial
   console after its first full boot.
